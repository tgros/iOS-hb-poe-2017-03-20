//
//  SandboxAPIService.swift
//  UnitTestDemo
//
//  Created by Apple on 04/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

enum SandboxAPIServiceError: Error {
    case internalError(Int)
    case networkError(Error)
}

class SandboxAPIService {
    
    let urlSession: URLSession
    
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    func hello(_ completionHandler: @escaping (String?, Error?) -> Void) {
        
        let task = urlSession.dataTask(with: URL(string:"http://aie-ut.getsandbox.com/hello")!) {
            data, response, error in

            guard error == nil else {
                completionHandler(nil, SandboxAPIServiceError.networkError(error!))
                return
            }
            
            guard let response = response as? HTTPURLResponse,
                      response.statusCode != 500 else {
                completionHandler(nil, SandboxAPIServiceError.internalError(500))
                return
            }
                
            
            completionHandler(String(data: data!, encoding: .utf8), nil)
            
        }
        
        task.resume()
        
    }
    
}
