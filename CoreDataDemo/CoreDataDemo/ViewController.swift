//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by Apple on 05/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBAction func doubleTap(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        // create entities
        var employee = NSEntityDescription.insertNewObject(forEntityName: "Person", into: context) as! Person
        
        employee.firstname = "Thomas"
        employee.lastname = "Gros"
        
        // save entities
        appDelegate.saveContext()
        
        context.perform {
            
            // fetch entities
            let employeesFetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
            employeesFetchRequest.predicate = NSPredicate(format: "firstname == %@", "Thomas")
            employeesFetchRequest.sortDescriptors = [NSSortDescriptor(key: "lastname", ascending: false)]
            
            
            //let employeesFetchRequest = NSFetchRequest<Person>(entityName: "Person")

            do {
                //let fetchedPersons = try context.execute(employeesFetchRequest) as! [Person]
                let fetchedPersons = try employeesFetchRequest.execute()
                
                print(fetchedPersons)
            } catch {
                fatalError("Failed to fetch employees: \(error)")
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

