//
//  QuotesTableTableViewController.swift
//  HelloWorld
//
//  Created by Apple on 22/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class QuotesTableTableViewController: UITableViewController {
    
//    var quotes: [Quote] = [
//        Quote(author: "thomas", body:"iOS c'est génial"),
//        Quote(author: "steve jobs", body:"Thomas a raison !")
//    ]
    
    var quotes: [Quote] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let path = Bundle.main.path(forResource: "quotes", ofType: "plist"),
           let data = NSArray(contentsOfFile: path) as? [Dictionary<String, String>] {
           
            quotes = data.map { Quote(author: $0["author"] ?? "unknown", body: $0["body"] ?? "unknown") }

//            for dict in (data as! [Dictionary<String, String>]) {
//                quotes.append(Quote(author: dict["author"] ?? "unknown", body: dict["body"] ?? "unknown"))
//            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // selection et segue, voir http://stackoverflow.com/questions/26278730/how-to-get-indexpath-in-prepareforsegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "quoteDetailSegue" {
            
            guard let selectedIndexPath = tableView.indexPathForSelectedRow else {
                print("no selection while preparing segue")
                return // do nothing
            }
            
            guard let quoteDetailController = segue.destination as? QuoteDetailViewController else {
                print("quoteDetail segue destination controller should be of type QuoteDetailViewController")
                return // do nothing
            }
            
            quoteDetailController.quote = quotes[selectedIndexPath.row]

            
            // solution 2
            //if let cell = sender as? QuotesTableViewCell {
            //    let indexPath = tableView.indexPath(for: cell)
            //    quoteDetailController.quote = quotes[indexPath.row]
            //}

        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quotes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ma-cell-type-1") as? QuotesTableViewCell
        
        cell?.quoteLbl.text = quotes[indexPath.row].body
        
        return cell!
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
