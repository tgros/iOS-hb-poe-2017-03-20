//
//  QuoteDetailViewController.swift
//  HelloWorld
//
//  Created by Apple on 23/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class QuoteDetailViewController: UIViewController {

    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var bodyLbl: UILabel!
    
    var quote: Quote?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        authorLbl.text = quote?.author
        bodyLbl.text = quote?.body
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
