//: Playground - noun: a place where people can play

import PlaygroundSupport
import UIKit


let session = URLSession.shared

// GET
let helloURL = URL(string: "http://aic-test.getsandbox.com/hello")!

let task = session.dataTask(with: helloURL) {
    (data, response, error) in
    guard error == nil else {
        print(error!)
        return
    }
    
    print(data!)
    print(String(data: data!, encoding: String.Encoding.utf8)!)
    
}

task.resume()

// POST
let usersURL = URL(string: "http://aic-test.getsandbox.com/users")!
var request = URLRequest(url: usersURL)
request.httpMethod = "POST"


let user = ["name":"thomas"]

// request.httpBody = body.data(using: String.Encoding.utf8)

request.httpBody = try? JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)

request.setValue("application/json", forHTTPHeaderField: "Content-Type")


let postTask = session.dataTask(with: request) {
    (data, response, error) in

//    print(String(data: data!, encoding: String.Encoding.utf8)!)
    
    guard error == nil else {
        print(error!)
        return
    }
    
    
}

postTask.resume()

// GET USERS
let getUsersTask = session.dataTask(with: usersURL) {
    (data, response, error) in
    
    guard error == nil else {
        print(error!)
        return
    }
    guard let data = data else {
        print("missing data")
        return
    }
    
    let users = try? JSONSerialization.jsonObject(with: data, options: []) as! [Any]
    
    if let users = users {
        print(users)
    }
    
}

getUsersTask.resume()



PlaygroundPage.current.needsIndefiniteExecution = true




















