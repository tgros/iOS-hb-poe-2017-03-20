//
//  ViewController.swift
//  Map
//
//  Created by Apple on 11/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    let sophia = CLLocationCoordinate2D(latitude: 43.6163539, longitude: 7.055221800000027)
    var sophiaImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string: "https://norois.revues.org/docannexe/image/1798/img-1-small480.jpg"),
            let data = try? Data(contentsOf: url),
            let image = UIImage(data: data) {
            
            
            let size = CGSize(width: 50, height: 50)
            UIGraphicsBeginImageContext(size)
            image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            
            sophiaImage = resizedImage
        }
        
        mapView.delegate = self
        
        mapView.showsScale = true
        mapView.showsCompass = true
        
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = sophia
        annotation.title = "sophia"
        annotation.subtitle = "antipolis"
        
        
        mapView.addAnnotation(annotation)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // mapView.setCenter(sophia, animated: true)
        mapView.setRegion(MKCoordinateRegionMakeWithDistance(sophia, 2000, 2000), animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
       
        if let view = mapView.dequeueReusableAnnotationView(withIdentifier: "pinview") {
            view.annotation = annotation
            return view
        }
        
        //let view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pinview")
        //         view.pinTintColor = UIColor.orange
        let view = MKAnnotationView(annotation: annotation, reuseIdentifier: "pinview")
        view.image = sophiaImage
            

        view.canShowCallout = true
        
        return view
    }
}















