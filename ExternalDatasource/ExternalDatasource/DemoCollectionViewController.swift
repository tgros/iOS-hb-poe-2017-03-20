//
//  DemoCollectionViewController.swift
//  ExternalDatasource
//
//  Created by Apple on 07/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class DemoCollectionViewController: UICollectionViewController {

    var dataSource: UICollectionViewDataSource? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ATTENTION LA DATASOURCE EST EN WEAK REFERENCE DANS COLLECTIONVIEW
        dataSource = ExternalDatasource1()
        self.collectionView?.dataSource = dataSource!
        self.collectionView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
