//
//  ExternalDatasource1.swift
//  ExternalDatasource
//
//  Created by Apple on 07/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit

class ExternalDatasource1: NSObject, UICollectionViewDataSource {
    
    let data = ["ds1-1", "ds1-2", "ds1-3"]
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BasicCell", for: indexPath)
        let textField = cell.subviews[0].subviews[0] as! UITextField
        textField.text = data[indexPath.row]
        
        return cell
    }
    
}
