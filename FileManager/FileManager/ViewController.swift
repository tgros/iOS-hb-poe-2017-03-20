//
//  ViewController.swift
//  FileManager
//
//  Created by Apple on 10/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let imageName = "my-image.jpg"
        
        if let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let imagePath = documentDirectoryURL.appendingPathComponent(imageName)
            
            
            if let imageURL = URL(string:"https://s-media-cache-ak0.pinimg.com/564x/a0/a1/51/a0a151c0bed9bf9fd39c7282dec154f5.jpg"),
                let data = try? Data(contentsOf: imageURL),
                let image = UIImage(data: data) {
                
                do {
                    try UIImageJPEGRepresentation(image, 1.0)?.write(to: imagePath)
                } catch {
                    fatalError("Can't write image \(error)")
                }
                
                
                guard let loadedImage = UIImage(contentsOfFile: imagePath.path) else {
                    fatalError("Can't read image")
                }
                
                self.imageView.image = loadedImage
                
            }
            
            
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

