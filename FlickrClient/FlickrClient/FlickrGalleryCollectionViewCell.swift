//
//  FlickrGalleryCollectionViewCell.swift
//  FlickrClient
//
//  Created by Thomas Gros on 02/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class FlickrGalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
