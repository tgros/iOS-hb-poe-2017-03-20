//
//  FlickrService.swift
//  FlickrClient
//
//  Created by Apple on 24/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

enum FlickrServiceError: Error {
    case missingAPIKey
    case networkError(error: Error)
    case apiMethodCall(message: String)
    case noDataFound
    case jsonDeserialization
    case invalidData
}


class FlickrService {
    
    static let sharedInstance: FlickrService = try! FlickrService()
    
    var session = URLSession.shared
    
    // https://www.flickr.com/services/api/explore/flickr.photos.search
    let API_KEY: String
    
    private init() throws {
        
        guard let config = Bundle.main.path(forResource: "config", ofType: "plist"),
              let myDict = NSDictionary(contentsOfFile: config) else {
                throw FlickrServiceError.missingAPIKey
            }
    
        API_KEY = myDict.value(forKey: "FLICKR_API_KEY") as! String
    }
    
    func search(with criteria: [String:String], _ completionHandler: @escaping ([FlickrPhoto]?, FlickrServiceError?) -> Void) {
        
        var query = [String:String]()
        for (param, value) in criteria {
            query[param] = value;
        }
        
        query["method"] = "flickr.photos.search"


        let queryURL = urlBuilder(query: query)
        
        let request = session.dataTask(with: queryURL) {
            data, response, error in
            
            print("API CALL: GET \(queryURL)")
            
            guard error == nil else {
                print("error calling FlickrService.search: \(error!)")
                completionHandler(nil, .networkError(error: error!))
                return
            }
            
            guard let data = data else {
                print("no data from FlickrService.search")
                completionHandler(nil, .noDataFound)
                return
            }
            
            
            guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any] else {
                print("cannot deserialize JSON")
                completionHandler(nil, .jsonDeserialization)
                return
            }
            
            
            guard let stat = jsonObject["stat"] as! String?, stat == "ok" else { // {"stat":"fail","code":3,"message":"Parameterless searches have been disabled. Please use flickr.photos.getRecent instead."}
                
                if let message = jsonObject["message"] as? String {
                    print("ERROR: api call don't returns ok but \(message)")
                    completionHandler(nil, .apiMethodCall(message: message))
                } else {
                    completionHandler(nil, .apiMethodCall(message: "no message provided"))
                }
                
                return
            }
            
            guard let photosParent = jsonObject["photos"] as? [String:Any],
                let photos = photosParent["photo"]  as? [[String:Any]] else {
                    print("ERROR: no photo properties found")
                    completionHandler(nil, .invalidData)
                    return
            }
            
            let flickrPhotos = photos.map {
                FlickrPhoto(
                    farm: String($0["farm"] as! Int),
                    id: $0["id"] as! String,
                    isfamily: String($0["isfamily"] as! Int),
                    isfriend: String($0["isfriend"] as! Int),
                    ispublic: String($0["ispublic"] as! Int),
                    owner: $0["owner"] as! String,
                    secret: $0["secret"] as! String,
                    server: $0["server"] as! String,
                    title: $0["title"] as! String,
                    oSecret: $0["title"] as? String)
            }
            
            completionHandler(flickrPhotos, nil)
            
        }
        
        request.resume()
        
    }
    
    
    private func urlBuilder(query: [String:String]) -> URL {
        var requestURL = "https://api.flickr.com/services/rest/?format=json&nojsoncallback=1&api_key=\(API_KEY)"
        
        for (param,value) in query {
            requestURL += "&" + param + "=" + value
        }

        return URL(string: requestURL)!
    }
}
