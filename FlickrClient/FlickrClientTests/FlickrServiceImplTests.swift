//
//  FlickrClientTests.swift
//  FlickrClientTests
//
//  Created by Apple on 24/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import XCTest
@testable import FlickrClient

class FlickrServiceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearch() {
        let asyncExpectation = expectation(description: "longRunningFunction")
        
        var photos: [FlickrPhoto]?
        var callError: FlickrServiceError?
        
        let service = FlickrService.sharedInstance
        service.search(with: ["tags": "landscape"]) {
            (result, flickrServiceError) in
            photos = result
            callError = flickrServiceError
            asyncExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error, "Something went horribly wrong")
            XCTAssertNil(callError, "Got an error calling flickr service")
            XCTAssertNotNil(photos)
            
            if let photos = photos {
                XCTAssert(photos.count > 0)
            }
            
            
        }
        
    }
    
//    func testExample() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
