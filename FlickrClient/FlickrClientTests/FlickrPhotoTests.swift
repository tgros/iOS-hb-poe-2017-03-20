//
//  FlickrPhotoTests.swift
//  FlickrClient
//
//  Created by Apple on 30/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import XCTest
@testable import FlickrClient

class FlickrPhotoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUrl() {
        let photo = FlickrPhoto(
            farm: "1234",
            id: "5678",
            isfamily: "0",
            isfriend: "0",
            ispublic: "0",
            owner: "thomas",
            secret: "112233",
            server: "111222333",
            title: "Wow, great shot",
            oSecret: nil)
        
        
        XCTAssertEqual(try! photo.url(), "https://farm\(photo.farm).staticflickr.com/\(photo.server)/\(photo.id)_\(photo.secret).jpg")
        XCTAssertEqual(try! photo.url(criteria: .t, ext: .gif), "https://farm\(photo.farm).staticflickr.com/\(photo.server)/\(photo.id)_\(photo.secret)_t.gif")

        //
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
}
